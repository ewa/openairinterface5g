#/*
# * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
# * contributor license agreements.  See the NOTICE file distributed with
# * this work for additional information regarding copyright ownership.
# * The OpenAirInterface Software Alliance licenses this file to You under
# * the OAI Public License, Version 1.1  (the "License"); you may not use this file
# * except in compliance with the License.
# * You may obtain a copy of the License at
# *
# *      http://www.openairinterface.org/?page_id=698
# *
# * Unless required by applicable law or agreed to in writing, software
# * distributed under the License is distributed on an "AS IS" BASIS,
# * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# * See the License for the specific language governing permissions and
# * limitations under the License.
# *-------------------------------------------------------------------------------
# * For more information about the OpenAirInterface (OAI) Software Alliance:
# *      contact@openairinterface.org
# */
#---------------------------------------------------------------------
#
# Dockerfile for the Open-Air-Interface BUILD service
#   Valid for Ubuntu 18.04
#
#---------------------------------------------------------------------

FROM ubuntu:bionic AS dist-packages-layer
ARG NEEDED_GIT_PROXY
ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Paris
ENV UHD_VERSION=3.15.0.0

#install developers pkg/repo
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get full-upgrade --yes && \
    DEBIAN_FRONTEND=noninteractive apt-get install --yes \
       #gcc needed for build_oai
       build-essential \
       psmisc \
       git \
       xxd \
       # python3-pip for conf template generation
       python3-pip

#The optional packages
RUN DEBIAN_FRONTEND=noninteractive apt-get install --yes \
    doxygen \
    libpthread-stubs0-dev \
    tshark \
    uml-utilities \
    iperf3 \
    libforms-bin \
    libforms-dev \
    xmlstarlet \
    python-dev \
    python-pip \
    python-pyroute2 \
    python python-numpy \
    python-scipy \
    python-matplotlib \
    ctags

#The packages from check_install_oai_software
RUN DEBIAN_FRONTEND=noninteractive apt-get install --yes \
    libgcrypt11-dev \
    guile-2.0-dev \
    automake \
    build-essential \
    cmake \
    ninja-build \
    pkg-config \
    git \
    libatlas-base-dev \
    libblas-dev \
    liblapack-dev \
    liblapacke-dev \
    libreadline-dev \
    libgnutls28-dev \
    libconfig-dev \
    libsctp-dev  \
    libssl-dev  \
    libtool  \
    libxslt1-dev \
    libtasn1-6-dev \
    patch \
    openssl \
    xxd \
    nettle-dev \
    nettle-bin

#Specifically do not have os-supplied UHD packages:
FROM dist-packages-layer AS clean-uhd
RUN DEBIAN_FRONTEND=noninteractive apt-get install --yes software-properties-common
RUN DEBIAN_FRONTEND=noninteractive apt-get purge --yes \
    uhd-host \
    uhd-soapysdr \
    libuhd-dev 
RUN /bin/bash -c 'UHDS=$(dpkg --get-selections "libuhd*" 2>/dev/null); [[ -z $UHDS ]] || exit 1'

#UHD from Ettus - we're skipping the check_install_usrp_uhd_driver nonsense
RUN apt-add-repository ppa:ettusresearch/uhd -y
# first line of packages is boost_libs_ubuntu in build_helper.  Does libusb need to be pinned at 1.0.0-dev?
RUN DEBIAN_FRONTEND=noninteractive apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install \
    libboost-chrono-dev libboost-date-time-dev libboost-filesystem-dev libboost-program-options-dev libboost-thread-dev libboost-test-dev libboost-regex-dev \ 
    libusb-1.0.0-dev \
    libuhd-dev \
    libuhd4.4.0 \
    uhd-host \
    python3-uhd


# #Debugging packages from ettus -- there's now one uhd-doc package to rule them all, apparently
# RUN DEBIAN_FRONTEND=noninteractive apt-get install -y liblz4-tool
# RUN ls -lah /etc/apt/sources.list.d/ && \
#     cat /etc/apt/sources.list.d/* && \
#     ls -lah /var/lib/apt/lists/ && \
#     /usr/bin/lz4 -dc < /var/lib/apt/lists/ppa.launchpad.net_ettusresearch_uhd_ubuntu_dists_bionic_main_binary-amd64_Packages.lz4 && \
#     apt-cache search uhd && \
#     exit 1


#Probably excessive
FROM clean-uhd AS pip-layer 
RUN pip3 install --ignore-installed pyyaml

#This is where we actually start building stuff
FROM pip-layer as ran-base

# In some network environments, GIT proxy is required
RUN /bin/bash -c "if [[ -v NEEDED_GIT_PROXY ]]; then git config --global http.proxy $NEEDED_GIT_PROXY; fi"

#create the WORKDIR
WORKDIR /oai-ran
COPY oaienv .
COPY cmake_targets/ cmake_targets/

RUN apt-get purge -y python-tk
#RUN apt-get autoremove

# Does mysterious things
RUN /bin/sh oaienv && \ 
    cd cmake_targets && \
    mkdir -p log && \
    ./build_oai --uhd-trust-packages --fail-missing-packages -k  -I -w USRP 
