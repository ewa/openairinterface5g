# Prerequisite Lists

This directory contains lists of packages required for various build
steps and configurations.

The intention is to provide a common source (DRY principle) for
listing packages to be installed _either_ a-priori (for example for
Docker builds) _or_ at build time through the build_oai script.

